<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Checkinout as CheckinoutResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Checkinout;
use Validator;

class CheckinoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->hasRole('admin')) {
            $checkinout = Checkinout::paginate();
        }else{
            $checkinout = Checkinout::where('user_id',$user->id)->paginate();
        }
        return CheckinoutResource::collection($checkinout);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'id' => [ 'required','unique:checkinouts'],
                'nik' => ['required'],
                'checktime' => ['required','date_format:Y-m-d H:i:s'],
                'checktype' => ['required'],
                'lat' => ['required'],
                'lng' => ['required'],
                'photo' => ['required','image']
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'code'=> 400,
                    'errors' => $validator->errors()
                ], 400);
        }

        if (!$request->file('photo')->isValid()) {
            return response()->json(
                [
                    'code'=> 400,
                    'errors' => 'photo not valid'
                ], 400);
        }
        $user = Auth::user();

        $checkinout = new Checkinout();
        $checkinout->id = $request->id;
        $checkinout->user_id = $user->id;
        $checkinout->nik = $request->nik;
        $checkinout->checktime = $request->checktime;
        $checkinout->checktype = $request->checktype;
        $checkinout->lat = $request->lat;
        $checkinout->lng = $request->lng;
        //photo disimpan ketika data tercreate di observer
        $checkinout->save();

        return new CheckinoutResource(Checkinout::find($request->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        if ($user->hasRole('admin')) {
            $checkinout = Checkinout::find($id);
        }else{
            $checkinout = Checkinout::where('id',$id)->where('user_id',$user->id)->first();
            if (!$checkinout) {
                $response = [
                    "code" => 401,
                    "success" => false,
                    "data" => [],
                    "message" => 'Unauthenticated'
                ];
                return response()->json($response, 401);
            }
        }
        return new CheckinoutResource($checkinout);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
