<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Priority;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthController extends Controller
{
    public $successStatus = 200;

    /**
     * @OA\Post(
     *      path="/api/v1/register",
     *      operationId="registerUser",
     *      tags={"Auth"},
     *      summary="Register",
     *      description="Register User",
     *      @OA\Parameter(
     *         name="accept",
     *         in="header",
     *         @OA\Schema(
     *             type="string",
     *             default="application/json"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="name",
     *         description="User Name",
     *         required=true,
     *         in="query",
     *         @OA\Schema(
     *             type="string",
     *             format="string"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="email",
     *         description="email user",
     *         required=true,
     *         in="query",
     *         @OA\Schema(
     *             type="string",
     *             format="email"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="password",
     *         description="password",
     *         required=true,
     *         in="query",
     *         @OA\Schema(
     *             type="string",
     *             format="password"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="c_password",
     *         description="confirmation password",
     *         required=true,
     *         in="query",
     *         @OA\Schema(
     *             type="string",
     *             format="password"
     *         )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     *     )
     *
     */
    public function register(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required',
                'email' => 'required|unique:users,email',
                'password' => 'required',
                'c_password' => 'required|same:password',
            ]
        );
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        
        $user = User::create($input);

        $getUser = User::where('email',$user->email)->first();
        $getUser['api_token'] =  $getUser->createToken('KHADIR API')->accessToken;
        $getUser->save();
        $getUser->assignRole('karyawan');

        $response = [
            "code" => 200,
            "success" => true,
            "data" => new UserResource($getUser),
            "message" => "Success"
        ];
        return response()->json($response, 200);
    }

    /**
     * @OA\Post(
     *      path="/api/v1/login",
     *      operationId="loginUser",
     *      tags={"Auth"},
     *      summary="Login",
     *      description="Login User",
     *      @OA\Parameter(
     *         name="email",
     *         description="email user",
     *         required=true,
     *         in="query",
     *         @OA\Schema(
     *             type="string",
     *             format="email"
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="password",
     *         description="password user",
     *         required=true,
     *         in="query",
     *         @OA\Schema(
     *             type="string",
     *             format="password"
     *         )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *       @OA\Response(response=400, description="Bad request"),
     *     )
     *
     */
    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $success['api_token'] =  $user->createToken('KHADIR API')->accessToken;

            $user = User::find($user->id);
            $user->api_token = $user->createToken('KHADIR API')->accessToken;
            $user->save();

            $response = [
                "code" => 200,
                "success" => true,
                "data" => new UserResource($user),
                "message" => "Success"
            ];
            return response()->json($response, 200);
        } else {
            $response = [
                "code" => 401,
                "success" => false,
                "data" => [],
                "message" => "Unauthorised"
            ];
            return response()->json($response, 401);
        }
    }

    public function getUser()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }
}
