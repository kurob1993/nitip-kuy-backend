<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\Project as ProjectResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Project;
use Validator;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ProjectResource::collection(Project::with(["projectSigns"])->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'kode' => [ 'required','unique:projects'],
                'nama' => ['required'],
                'alamat' => ['required'],
                'lat' => ['required'],
                'lng' => ['required'],
                'radius' => ['required','numeric'],
                'status' => ['required']
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'code'=> 400,
                    'errors' => $validator->errors()
                ], 400);
        }

        $project = new Project();
        $project->kode = $request->kode;
        $project->nama = $request->nama;
        $project->alamat = $request->alamat;
        $project->lat = $request->lat;
        $project->lng = $request->lng;
        $project->radius = $request->radius;
        $project->status = $request->status;
        $project->save();

        return new ProjectResource($project);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        return new ProjectResource(Project::with(["projectSigns"])->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
