<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ProjectSign as ProjectSignResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ProjectSign;
use Validator;

class ProjectSignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ProjectSignResource::collection(ProjectSign::with(["project"])->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'project_id' => [ 
                    'required',
                    'unique:project_signs,project_id,null,id,user_id,'.$request->user_id,'exists:projects,id'
                ],
                'user_id' => [
                    'required',
                    'unique:project_signs,user_id,null,id,project_id,'.$request->project_id
                ]
            ]
        );

        if ($validator->fails()) {
            return response()->json(
                [
                    'code'=> 400,
                    'errors' => $validator->errors()
                ], 400);
        }

        $project = new ProjectSign();
        $project->project_id = $request->project_id;
        $project->user_id = $request->user_id;
        $project->save();

        return new ProjectSignResource($project);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new ProjectSignResource(ProjectSign::with(["project"])->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
