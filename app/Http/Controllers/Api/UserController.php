<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\UserResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
     /**
     * @OA\Get(
     *      tags={"User"},
     *      path="/api/v1/users",
     *      description="List User",
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *     ),
     *     @OA\Response(response=400, description="Bad request"),
     *     security={
     *         {"api_key": {}}
     *     }
     * )
     */
    public function index()
    {
      return UserResource::collection(User::paginate());
    }
}
