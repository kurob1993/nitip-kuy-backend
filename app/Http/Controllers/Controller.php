<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @OA\Info(
     *      version="0.0.1",
     *      title="@Nitip-Kuy Api Documentation",
     *      description="Api Documentation for @Nitip-Kuy",
     * )
     */

     /**
     * @OA\SecurityScheme(
     *     type="apiKey",
     *     name="Authorization",
     *     in="header",
     *     securityScheme="api_key",
     *     description="Bearer <api_key>"
     * )
     */
}
