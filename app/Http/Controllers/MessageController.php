<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Message;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();
        $message = Message::where('user_id',$user->id)
        ->orderBy('created_at','desc')
        ->paginate(10);

        return view('message', ['message' => $message]);
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        $validator = Validator::make(
            $request->all(),
            [
                'number' => [
                    'required','numeric','digits_between:12,15',
                    function ($attribute, $value, $fail) {
                        if (!Str::of($value)->is('62*')) {
                            $fail($attribute.' is invalid. ext: 62**********');
                        }
                    }
                ],
                'text' => ['required','max:1000']
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if (!$user->canSendMessage()) {
            return redirect()->back()->withErrors('Your quota is limit');
        }
        
        $success =  $request->input();
        $success['user_id'] =  $user->id;
        $success['stage_id'] =  1;

        Message::create($success);

        return redirect()->back()->with('status', 'data entered successfully');
    }

    public function show()
    {
        if (!Auth::User()->is_admin) {
            return redirect('home')->with('status', 'You do not have access');
        }
        
        $message = Message::orderBy('created_at','desc')->paginate(10);
        return view('message', ['message' => $message]);
    }

}
