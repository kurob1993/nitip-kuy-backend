<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function projectSigns()
    {
        return $this->hasMany('App\ProjectSign');
    }
}
