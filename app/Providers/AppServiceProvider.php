<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Observers\CheckinoutObserver;
use App\Checkinout;
Use Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Checkinout::observe(CheckinoutObserver::class);
    }
}
