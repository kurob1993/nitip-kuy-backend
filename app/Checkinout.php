<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkinout extends Model
{
    public $incrementing = false;
    protected $primaryKey = 'id';
    protected $keyType = 'string';
}
