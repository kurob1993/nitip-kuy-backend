<?php

namespace App\Observers;

use Illuminate\Http\Request;
use App\Checkinout;

class CheckinoutObserver
{
    /**
     * Handle the checkinout "created" event.
     *
     * @param  \App\Checkinout  $checkinout
     * @return void
     */
    public function created(Checkinout $checkinout)
    {
        $nik = Request()->nik;
        $checktime = Request()->checktime;
        $checktype = Request()->checktype;
        $date = date('YmdHis',strtotime($checktime));
        $ext = Request()->file('photo')->extension();

        $nameFile = $nik.'-'.$checktype.'-'.$date.'.'.$ext;
        Request()->photo->storeAs('public/images/'.$nik, $nameFile);
        
        $checkinout->photo = 'public/images/'.$nik.'/'.$nameFile;
        $checkinout->save();
    }

    /**
     * Handle the checkinout "updated" event.
     *
     * @param  \App\Checkinout  $checkinout
     * @return void
     */
    public function updated(Checkinout $checkinout)
    {
        //
    }

    /**
     * Handle the checkinout "deleted" event.
     *
     * @param  \App\Checkinout  $checkinout
     * @return void
     */
    public function deleted(Checkinout $checkinout)
    {
        //
    }

    /**
     * Handle the checkinout "restored" event.
     *
     * @param  \App\Checkinout  $checkinout
     * @return void
     */
    public function restored(Checkinout $checkinout)
    {
        //
    }

    /**
     * Handle the checkinout "force deleted" event.
     *
     * @param  \App\Checkinout  $checkinout
     * @return void
     */
    public function forceDeleted(Checkinout $checkinout)
    {
        //
    }
}
