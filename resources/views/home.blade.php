@extends('layouts.app')

@push('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/prism/1.19.0/themes/prism.min.css" rel="stylesheet" />
@endpush

@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.19.0/components/prism-core.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.19.0/plugins/autoloader/prism-autoloader.min.js"></script>
@endpush

@section('content')
@include('components.title.index',["title" => "Home"])
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <div class="col-md">
                <h4>Welcome</h4>
            </div>
        </div>
    </div>
</div>
@endsection