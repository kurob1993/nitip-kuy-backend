<li class="user-pro">
    <a href="#" class="waves-effect"><img src="../plugins/images/users/varun.jpg" alt="user-img"
            class="img-circle"> <span class="hide-menu">
                {{ Auth::user()->name }} <span class="fa arrow"></span></span>
    </a>
    <ul class="nav nav-second-level">
        <li><a href="javascript:void(0)"><i class="ti-user"></i> My Profile</a></li>
        <li><a href="javascript:void(0)"><i class="ti-wallet"></i> My Balance</a></li>
        <li><a href="javascript:void(0)"><i class="ti-email"></i> Inbox</a></li>
        <li><a href="javascript:void(0)"><i class="ti-settings"></i> Account Setting</a></li>
        <li>
            <a href="{{ route('logout') }}"
                onclick=" event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="fa fa-power-off"></i> Logout
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </a>
        </li>
    </ul>
</li>