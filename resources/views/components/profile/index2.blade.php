<li class="dropdown">
    <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img
            src="../plugins/images/users/varun.jpg" alt="user-img" width="36" class="img-circle"><b
            class="hidden-xs">{{ Auth::user()->name }}</b> </a>
    <ul class="dropdown-menu dropdown-user scale-up">
        <li><a href="#"><i class="ti-user"></i> My Profile</a></li>
        <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li>
        <li><a href="#"><i class="ti-email"></i> Inbox</a></li>
        <li role="separator" class="divider"></li>
        <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li>
        <li role="separator" class="divider"></li>
        <li>
            <a href="{{ route('logout') }}"
                onclick=" event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="fa fa-power-off"></i> Logout
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </a>
        </li>
    </ul>
</li>