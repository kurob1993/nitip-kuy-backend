<pre>
<code class="language-http">
   GET /api/v1/maps/location/106.034481/-5.998866 HTTP/1.1
   Host: 127.0.0.1:8000
   Accept: application/json  
   Authorization: Bearer {{ $user->api_token }}
</code>
</pre>