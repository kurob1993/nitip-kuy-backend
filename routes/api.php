<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v1')->group(function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('register', 'Api\AuthController@register');

    Route::group(['middleware' => ['auth:api', 'role:admin'] ], function () {        
        Route::resource('projects', 'Api\ProjectController')->only('index','show','store');
        Route::resource('project-signs', 'Api\ProjectSignController')->only('index','show','store');
        Route::get('maps/location/{long?}/{lat?}', 'Api\Maps@location');
        Route::get('users', 'Api\UserController@index');
    });

    Route::group(['middleware' => ['auth:api', 'role:admin|karyawan'] ], function () {        
        Route::resource('checkinouts', 'Api\CheckinoutController')->only('index','show','store');
    });

});
