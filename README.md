## KHadir

<hr>
<p>Install dependensi program</p>

```
composer install
```
<hr>
<p>Membuat file environment</p>

``` 
cp .env.example .env
php artisan key:generate

Atur koneksi database pada file .env
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=khadir
DB_USERNAME=root
DB_PASSWORD=

php artisan config:cache
```

<hr>
Migrasi database

```
php artisan migrate
php artisan passport:install --length=512 --force

php artisan db:seed
php artisan config:cache
```

<hr>
Publish file to public

```
php artisan storage:link
```

<hr>
<p>-- Opsional --</p>
<p>Update dependensi program</p>

```
composer update
```

<hr>
<p>For Generate Api Documentation</p>
http://127.0.0.1:8000/api/documentation

```
php artisan l5-swagger:generate
```