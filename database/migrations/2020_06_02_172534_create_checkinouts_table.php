<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckinoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkinouts', function (Blueprint $table) {
            $table->string('id');
            $table->string('user_id'); //sebagain nik
            $table->dateTime('checktime');
            $table->enum('checktype',['0','1','2','3','4','5']);
            $table->string('lat');
            $table->string('lng');
            $table->string('photo')->nullable();
            $table->timestamps();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkinouts');
    }
}
